import React, {Component} from 'react';


export default class NodeInfo extends Component {
    state = {}

    componentDidMount() {
        this.props.nodes.sub(selectedNode => {
            this.setState({ selectedNode })
            this.convertCurrency(selectedNode)
        })
        this.props.currencyConversionRate.sub(currencyConversionRate => {
            this.setState({ currencyConversionRate: currencyConversionRate })
        })
        this.props.currency.sub(currency => {
            this.setState({currency: currency})
        })
    }

    convertCurrency(selectedNode) {
        var newNetValue = selectedNode.netValue
        var rate = this.state.currencyConversionRate
        if (this.state.currency === "$") {
            newNetValue = parseFloat(rate * newNetValue);
        }
        this.setState({netValue: newNetValue})
    }

    render() {
      return this.state.selectedNode
          ? this.renderSelectedNode()
          : NodeInfo.renderNoneSelected()
    }

    renderSelectedNode() {
        return (
            <div className="selected-node">
                <h4>{this.state.selectedNode.id}</h4>
<table id="customers">
                <tr>
                    <td>Outgoing Transactions</td>
                    <td>{this.state.selectedNode.transactionsFromCount}</td>
                </tr>
                <tr>
                    <td>Incoming Transactions</td>
                    <td>{this.state.selectedNode.transactionsToCount}</td>
                </tr>
                <tr>
                    <td>Node Net Value</td>
                    <td>({this.state.currency}){this.state.netValue}</td>
                </tr>
</table>
            </div>
        )
    }

    static renderNoneSelected() {
        return (
            <div className="selected-node">
                <div>No Node selected yet</div>
            </div>
        )
    }
}

//<div className="price-hover" onClick={this.onValueClick}>{this.state.selectedNode.currency}{this.state.selectedNode.netValue}</div>
